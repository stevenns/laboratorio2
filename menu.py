from diccionario import *
def menu():
    print("MENU\n"
          "1) registre una persona.\n"
          "2) Recorrer personas registradas.\n"
          "3) eliminar persona con la cedula.\n"
          "4) consultar persona con la cedula.\n"
          "5) cambiar persona por el numero de cedula.\n"
          "6) Salir.")
    opcion = input("Digite la opcion que desea realizar:")
    if opcion == "1":
        insertarPersona()
        menu()
    elif opcion == "2":
        recorrerPersonas()
        menu()
    elif opcion == "3":
        EliminarPersona()
        menu()
    elif opcion == "4":
        consultarCedula()
        menu()
    elif opcion == "5":
        cambiar()
        menu()
    elif opcion == "6":
        documentacion()
        return
    else:
        print("opcion no valida, por favor elija una que se encuentre en la lista")
menu()
